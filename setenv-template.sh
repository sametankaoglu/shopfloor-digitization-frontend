##########################################################################################
# General settings
export HOST_NAME="vm-CHANGE_ME.westeurope.cloudapp.azure.com"
export APP_RUNTIME="java"
export PCS_KEYVAULT_NAME="keyvault-CHANGE_ME"
export PCS_AAD_APPID="CHANGE_ME"
export PCS_AAD_APPSECRET="CHANGE_ME"

##########################################################################################
# Settings for device simulation
export PCS_AUTH_REQUIRED="false"
export PCS_CORS_WHITELIST=""
export PCS_AUTH_ISSUER="https://sts.windows.net/CHANGE_ME"
export PCS_AUTH_AUDIENCE=""
export PCS_TWIN_READ_WRITE_ENABLED="false"
export PCS_CACHE="/tmp/azure/iotpcs/.cache"
export PCS_LOG_LEVEL="Warn"
export PCS_TWIN_READ_WRITE_ENABLED="false"
export PCS_IOTHUB_CONNSTRING="HostName=iothub-CHANGE_ME.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=CHANGE_ME"
export PCS_STORAGEADAPTER_WEBSERVICE_URL="http://storageadapter:9022/v1"
export PCS_STORAGEADAPTER_DOCUMENTDB_CONNSTRING="AccountEndpoint=https://documentdb-CHANGE_ME.documents.azure.com:443/;AccountKey=CHANGE_ME;"
export PCS_AZURE_STORAGE_ACCOUNT="DefaultEndpointsProtocol=https;AccountName=storageCHANGE_ME;AccountKey=CHANGE_ME;EndpointSuffix=core.windows.net"
export PCS_SUBSCRIPTION_ID="CHANGE_ME"
export PCS_RESOURCE_GROUP="fischertechnik"
export PCS_IOHUB_NAME="iothub-CHANGE_ME"
export PCS_SEED_TEMPLATE="default"
export PCS_C2D_METHODS_ENABLED="false"
export PCS_SOLUTION_TYPE="RemoteMonitoring"
export PCS_DIAGNOSTICS_WEBSERVICE_URL="http://diagnostics:9006/v1"


##########################################################################################
# Development settings, don't change these in Production
# You can run 'start.sh --unsafe' to temporarily disable Auth and Cross-Origin protections

# Format: true | false
# empty => Auth required
export PCS_AUTH_REQUIRED=""

# Format: { 'origins': ['*'], 'methods': ['*'], 'headers': ['*'] }
# empty => CORS support disabled
export PCS_CORS_WHITELIST=""

