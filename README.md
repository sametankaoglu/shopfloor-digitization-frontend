# Industry 4.0 Runtime

This repo provides a runtime configuration for the microservices which
consitute the Microsoft Acure Indutry 4.0 Accelerator. It is assumed that
the Microsoft Azure components (IoT Hub, Cosmos DB, ...) are already
created.

## Connecting and Setting up the Environment

Connect to the VM using SSH. The ports are used for managing the containers 
through your browser (Portainer.io).

```
ssh -L 19000:127.0.0.1:9000 -L 18000:127.0.0.1:8000 $VM_USER@$VM_HOSTNAME
```

When connected to the VM, set the environment by running:

```
. ./setenv.sh
```

In case `setenv.sh` does not exist it can be created from the template `setenv-template.sh`.

## Managing the Container Lifecyle

Note: The VM has been equipped with a [Portainer](http://portainer.io) installation which 
allows manageing the containers through a web browser, Key features of Portainer include:

* Starting and stopping cintainers
* Accessing the container logs
* Accessing the container shell
* And many more...

Installation of Portainer was done as follows:

```
docker volume create portainer_data
docker run -d -p 8000:8000 -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
```

When ssh connection is established as shown above, the Portainer UI is available 
thorough this [link](http://localhost:19000).

### Create the Containers

```
sudo -E ./create.sh 
```

### Start the Containers

```
sudo -E ./start.sh 
```

There is a manual workaround required for the `webui` container, as the update of the
`webui-config.js` fails. As the `webui` will be replaced by a custom coniguration,
there is no need to fix the root cause. The workaround includes four steps:

* Step 1: Connect to the container with `sudo -E docker-compose exec webui /bin/bash`
* Step 2: Comment our the lines before `Start Server` in `run.sh`
* Step 3: Fix values for `authRequired` and `tenant` in `build/webui-config.sh`
* Step 4: Restart the `webui` container

### Stop the Containers

```
sudo -E ./stop.sh 
```

### Remove the Containers

```
sudo -E delete.sh 
```

### Show the Container Logs

```
sudo -E docker-compose logs -f
```

### Check the Microservice Health

Run the below commands from a container which is connectd to the same network
as the microservices, e.g.

```
sudo docker run -it --rm --network=app_default_net arunvelsriram/utils /bin/bash
```

```
curl --request GET http://auth:9001/v1/status
curl --request GET http://storageadapter:9022/v1/status
curl --request GET http://diagnostics:9006/v1/status
curl --request GET http://devicesimulation:9003/v1/status
curl --request GET http://telemetry:9004/v1/status
curl --request GET http://conig:9005/v1/status
curl --request GET http://iothubmanager:9002/v1/status
curl --request GET http://asamanager:9024/v1/status
```

### Start the Device Simulation

The API specification can be found [here](https://github.com/Azure/device-simulation-dotnet/wiki/%5BAPI-Specifications%5D-Simulations)

Run the below commands from a container which is connectd to the same network
as the microservices, e.g.

```
sudo docker run -it --rm --network=app_default_net arunvelsriram/utils /bin/bash
```

```
curl --location --request \
    POST 'http://devicesimulation:9003/v1/simulations?template=default' \
    --header 'Content-Type: application/json' -d ""
curl --location --request \
    GET 'http://devicesimulation:9003/v1/simulations'
```

## Launch the WebUI

Point your Browser to [here](https://fischertechnik-CHANGE_ME.azurewebsites.net)

## Refernces
[Microsoft Acure Indutry 4.0 accelerator](https://github.com/Azure/remote-monitoring-services-java)




