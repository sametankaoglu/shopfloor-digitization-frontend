// Copyright (c) Microsoft. All rights reserved.

// This file is rewritten during the deployment.
// Values can be changed for development purpose.
// The file is public, so don't expect secrets here.

var DeploymentConfig = {
    authEnabled: false,
    authType: 'aad',
    aad : {
      tenant: 'CHANGE_ME',
      appId: 'CHANGE_ME',
      instance: 'https://login.microsoftonline.com/'
    }
  }
